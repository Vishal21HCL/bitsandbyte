package com.bitsandbyte.home.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bitsandbyte.home.model.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer>{

}
