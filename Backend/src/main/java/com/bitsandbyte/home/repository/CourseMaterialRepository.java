package com.bitsandbyte.home.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bitsandbyte.home.model.CourseMaterial;

@Repository
public interface CourseMaterialRepository extends CrudRepository<CourseMaterial, Integer>{
       
}
