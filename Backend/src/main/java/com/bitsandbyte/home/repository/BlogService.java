package com.bitsandbyte.home.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bitsandbyte.home.model.Blog;

@Service
public class BlogService {
	@Autowired
	private BlogRepository blogRepository;

	// create blog
	public boolean createBlog(Blog blog) {
		if (this.blogRepository.existsById(blog.getBlogId())) {
			return false;
		}
		this.blogRepository.save(blog);
		return true;
	}

	// delete blog
	public boolean deteleBolg(int blogId) {
		if (this.blogRepository.existsById(blogId)) {
			this.blogRepository.deleteById(blogId);
			return true;
		}
		return false;
	}

	// update blog
	public boolean updateBlog(Blog blog) {
		if (this.blogRepository.existsById(blog.getBlogId())) {
			this.blogRepository.save(blog);
			return true;
		}
		return false;
	}
	
	//approve blog
	
	public void approveBlog(int id) {
		this.blogRepository.approveBlogById(id);
	}
	
	public int countAllBlog() {
		return (int) this.blogRepository.count();

}
	
	public int countPendingBlog() {
		return (int) this.blogRepository.count();

}

}
