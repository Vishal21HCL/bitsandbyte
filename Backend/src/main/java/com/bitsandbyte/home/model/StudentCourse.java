package com.bitsandbyte.home.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class StudentCourse {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int studentCourseId;

	@ManyToOne(fetch = FetchType.EAGER)
	private Student student;

	@OneToOne
	private Course course;

	public StudentCourse() {
		super();
	}

	public StudentCourse(Student student, Course course) {
		super();
		this.student = student;
		this.course = course;
	}

	public int getStudentCourseId() {
		return studentCourseId;
	}

	public void setStudentCourseId(int studentCourseId) {
		this.studentCourseId = studentCourseId;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	@Override
	public String toString() {
		return "StudentCourse [studentCourseId=" + studentCourseId + ", student=" + student + ", course=" + course
				+ "]";
	}

	
}
