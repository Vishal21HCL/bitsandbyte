package com.bitsandbyte.home.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "material")
public class CourseMaterial {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String title;
	private String description;
	private String materialUrl;
	private int deleteType;
	
	@ManyToOne
	@JsonBackReference
	private Course course;

	public CourseMaterial() {
		super();
	}

	public CourseMaterial(String title, String description, String materialUrl, int deleteType, Course course) {
		super();
		this.title = title;
		this.description = description;
		this.materialUrl = materialUrl;
		this.deleteType = deleteType;
		this.course = course;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMaterialUrl() {
		return materialUrl;
	}

	public void setMaterialUrl(String materialUrl) {
		this.materialUrl = materialUrl;
	}

	public int getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(int deleteType) {
		this.deleteType = deleteType;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	@Override
	public String toString() {
		return "CourseMaterial [id=" + id + ", title=" + title + ", description=" + description + ", materialUrl="
				+ materialUrl + ", deleteType=" + deleteType + ", course=" + course + "]";
	}
	
	

}
