package com.bitsandbyte.home.controller;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bitsandbyte.home.model.Role;
import com.bitsandbyte.home.model.User;
import com.bitsandbyte.home.model.UserRole;
import com.bitsandbyte.home.repository.UserRepository;
import com.bitsandbyte.home.service.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	// api used for creating user
	@PostMapping("/")
	public User createUser(@RequestBody User user) throws Exception {
		// Encoding password with bycryptPasswordEncoder
		user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));
		Set<UserRole> roles = new HashSet<UserRole>();

		Role role = new Role(11L, "Normal");

		UserRole userRole = new UserRole(user, role);
		roles.add(userRole);
		return this.userService.createUser(user, roles);
	}

	@PostMapping("/admin")
	public User createAdmin(@RequestBody User user) throws Exception {

		// Encoding password with bycryptPasswordEncoder
		user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));

		Set<UserRole> roles = new HashSet<UserRole>();

		Role role = new Role(10L, "Admin");

		UserRole userRole = new UserRole(user, role);
		roles.add(userRole);
		return this.userService.createUser(user, roles);
	}

	// api used for getting user by username
	@GetMapping("/{username}")
	public User getUserByUsername(@PathVariable String username) {
		return this.userRepository.findByUsername(username);
	}
	
	
	//api used for getting user by userId
	@GetMapping("/getUserId/{userId}")
	public Optional<User> getUserById(@PathVariable Long userId) {
		return this.userRepository.findById(userId);
	}
	

	// api used for getting user by username
	@DeleteMapping("/{userId}")
	public ResponseEntity<?> deleteUserByUsername(@PathVariable Long userId) {
		if (this.userService.deleteUserById(userId)) {
			return ResponseEntity.ok("User deleted succsessfuly");
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong!, ");
	}

	@PutMapping("/update")
	public User updateUser(@RequestBody User user) throws Exception {
		Set<UserRole> roles = new HashSet<UserRole>();
		Role role = new Role(11L, "Normal");
		UserRole userRole = new UserRole(user, role);
		roles.add(userRole);
		return this.userService.updateUser(user, roles);
	}
}
