package com.bitsandbyte.home.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bitsandbyte.home.model.Messages;
import com.bitsandbyte.home.model.ResponsePage;
import com.bitsandbyte.home.model.StudentCourse;
import com.bitsandbyte.home.repository.StudentCourseRepository;
import com.bitsandbyte.home.service.StudentCourseService;


@RestController
@CrossOrigin
@RequestMapping("/studentCourse")
public class StudentCourseController {
	
	@Autowired
	private StudentCourseRepository studentCourseRespository;
	
	@Autowired
	private StudentCourseService studentCourseService;
	
	@PostMapping("/addCourse")
	public ResponseEntity<?> addCourse(@RequestBody StudentCourse studentCourse){
		if(this.studentCourseService.studentCourseIds(studentCourse)) {
			return ResponseEntity.ok().body(new ResponsePage(Messages.SUCCESS, "You have added course"));
		}else {
			return  ResponseEntity.badRequest().body(new ResponsePage(Messages.FAILURE, "Something went wrong"));
			
		}
		
	}
	

}
