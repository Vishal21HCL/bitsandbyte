package com.bitsandbyte.home.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bitsandbyte.home.helper.FileUploadHelper;
import com.bitsandbyte.home.model.Address;
import com.bitsandbyte.home.model.Course;
import com.bitsandbyte.home.model.Messages;
import com.bitsandbyte.home.model.ResponsePage;
import com.bitsandbyte.home.model.Student;
import com.bitsandbyte.home.model.StudentCourse;
import com.bitsandbyte.home.repository.AddressRepository;
import com.bitsandbyte.home.repository.StudentRepository;
import com.bitsandbyte.home.service.AddressService;
import com.bitsandbyte.home.service.StudentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.bytebuddy.asm.Advice.This;

@RestController
@CrossOrigin
@RequestMapping("/student")
public class StudentController {

	/**
	 * @see This is the student controller
	 * @author Vishal Bharti
	 * @since 28-Jan-2022
	 * 
	 */
	@Autowired
	private FileUploadHelper fileUploadHelper;
	@Autowired
	private StudentService studentService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private AddressRepository addressRespository;
	
	@PostMapping("/registerStudent")
	/**
	 * @see this api is used for register new student
	 * @param student
	 * @return Response entity type message
	 */
	public ResponseEntity<?> studentRegister(@RequestParam(value = "profile", required = false) MultipartFile profile,
			@RequestParam(value = "certificate", required = false) MultipartFile certificate,
			@RequestPart(name = "student") String studentString) throws JsonMappingException, JsonProcessingException {
		System.out.println(studentString);
		Student student = new ObjectMapper().readValue(studentString, Student.class);
		Student student1 = new Student();

		student1.setStudentSr(student.getStudentSr());
		student1.setStudentName(student.getStudentName());
		student1.setBirthDay(student.getBirthDay());
		student1.setEmail(student.getEmail());
		student1.setFatherName(student.getFatherName());
		student1.setMotherName(student.getMotherName());
		student1.setGender(student.getGender());
		student1.setMobile(student.getMobile());
		student1.setGuardianMobile(student.getGuardianMobile());
		student1.setBlood(student.getBlood());
		student1.setStatus(student.getStatus());
		student1.setBirthDay(student.getBirthDay());
		student1.setPassword(student.getPassword());
		

		// uploading student profile picture
		boolean flag = this.fileUploadHelper.uploadFile(profile);

		if (flag) {
			String imgUrl = ServletUriComponentsBuilder.fromCurrentContextPath().path("/image/")
					.path(profile.getOriginalFilename()).toUriString();
			student1.setStudentProfile(imgUrl);
			;

		}

		// uploading student certificate
		boolean flagCertificate = this.fileUploadHelper.uploadFile(certificate);

		if (flagCertificate) {
			String certificateUrl = ServletUriComponentsBuilder.fromCurrentContextPath().path("/image/")
					.path(profile.getOriginalFilename()).toUriString();
			student1.setStudentCertificate(certificateUrl);
			;

		}
      System.out.println(student1);
		// saving student details in database
		if (this.studentService.insertStudent(student1)) {
			Student registeredStudent = this.studentRepository.findByStudentSr(student1.getStudentSr());
			for (Address address : student.getAddress()) {
				address.setStudent(registeredStudent);
				this.addressService.insertAddress(address);
			}
			
			StudentCourse studentCourse = new StudentCourse();
			Student student2 = new Student();
			Course course2 = new Course();
			course2.setId(student.getCousre().getStudentCourseId());
			student2.setId(registeredStudent.getId());
			
			studentCourse.setStudent(student2);
			studentCourse.setCourse(course2);
			
			
			return ResponseEntity.ok()
					.body(new ResponsePage(Messages.SUCCESS, "You have successfuly registered student!"));

		}
		return ResponseEntity.badRequest()
				.body(new ResponsePage(Messages.FAILURE, "This student is already registered"));

	}

	/**
	 * @see This api is used for delete student by student id
	 * @param studentId
	 * @return Response entity type message
	 */
	@DeleteMapping("deleteStudent/{studentId}")
	public ResponseEntity<?> deleteStudent(@PathVariable String studentId) {
		this.studentRepository.deleteByStudentId(studentId);
		return ResponseEntity.ok("Student deleted successfuly!");
	}

	@GetMapping("getStudent")
	public ResponseEntity<List<Student>> getAllStudent() {
		List<Student> students = (List<Student>) this.studentRepository.findAll();
		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
	}

	@GetMapping("getStudentBySr/{studentId}")
    public ResponseEntity<Student> studentById(@PathVariable String studentId){
		Student student = this.studentRepository.findByStudentSr(studentId);
		return new ResponseEntity<Student>(student, HttpStatus.OK);
	}
	
//Get student details by Student Id
	
	@GetMapping("getStudentById/{studentId}")
    public ResponseEntity<?> studentById(@PathVariable int studentId){
	Optional<Student> student = this.studentRepository.findById(studentId);
	return ResponseEntity.ok(student);
	}

	
	
	@PostMapping("/studentLogin")
	public ResponseEntity<?> studentLogin(@RequestBody Student student) {
		if (this.studentService.studentLogin(student)) {
			System.out.println("Student login function call");
			return ResponseEntity.ok()
					.body(new ResponsePage(Messages.SUCCESS, "You have successfuly login as student!"));

		}
		return ResponseEntity.badRequest()
				.body(new ResponsePage(Messages.FAILURE, "username or password is wrong!"));
	}
	
	@GetMapping("getAddress")
	public ResponseEntity<List<Address>> getAllAddress() {
		List<Address> address = (List<Address>) this.addressRespository.findAll();
		return new ResponseEntity<List<Address>>(address, HttpStatus.OK);
	}

	
	
}
