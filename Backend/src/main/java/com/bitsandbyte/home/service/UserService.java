package com.bitsandbyte.home.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bitsandbyte.home.model.User;
import com.bitsandbyte.home.model.UserRole;
import com.bitsandbyte.home.repository.RoleRepository;
import com.bitsandbyte.home.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	//creating user
	public User createUser(User user, Set<UserRole> userRoles) throws Exception {
		User newUser = new User();
		if (this.userRepository.existsByUsername(user.getUsername())) {
			System.out.println("User is already exist!");
			throw new Exception("User is already exist!");
		} else {
			for (UserRole ur : userRoles) {
				this.roleRepository.save(ur.getRole());
			}

			user.getUserRole().addAll(userRoles);
			newUser = this.userRepository.save(user);
		}
		return newUser;
	}
	
	// delete user from 
	public boolean deleteUserById(Long userId) {
		this.userRepository.deleteById(userId);
		return true;
	}
	
	// update user
	public User updateUser(User user,Set<UserRole> userRoles) throws Exception {
		User updatedUser = new User();
		if (this.userRepository.existsById(user.getId())) {
			for (UserRole ur : userRoles) {
				this.roleRepository.save(ur.getRole());
			}

			user.getUserRole().addAll(userRoles);
			updatedUser = this.userRepository.save(user);
			
		} else {
			System.out.println("User does not exist!");
			throw new Exception("User does not exist!");
		}
		return updatedUser;
	}
	
}
