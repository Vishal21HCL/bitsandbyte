package com.bitsandbyte.home.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bitsandbyte.home.model.Student;
import com.bitsandbyte.home.repository.StudentRepository;

@Service
@Transactional
public class StudentService {
	@Autowired
	private StudentRepository studentRepository;

	//register new student
	public boolean insertStudent(Student student) {
		if (this.studentRepository.existsByStudentSr(student.getStudentSr())) {
			return false;
		}
		this.studentRepository.save(student);
		return true;
	}

	//student login
	public boolean studentLogin(Student student) {
		List<Student> students = (List<Student>) this.studentRepository.findAll();
		if (this.studentRepository.existsByStudentSr(student.getStudentSr())) {
			for (Student stud : students) {
				if ( stud.getPassword().equals(student.getPassword())){
					return true;
				}
			}
			return false;
		}
		return false;
	}

}
