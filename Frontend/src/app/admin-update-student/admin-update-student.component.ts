import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSidenav } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { delay } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Address } from '../model/Address';
import { Course } from '../model/Course';
import { Student } from '../model/Student';
import { StudentCourse } from '../model/StudentCourse';
import { CourseService } from '../service/course.service';
import { StudentCourseService } from '../service/student-course.service';
import { StudentService } from '../service/student.service';

@Component({
  selector: 'app-admin-update-student',
  templateUrl: './admin-update-student.component.html',
  styleUrls: ['./admin-update-student.component.css']
})
export class AdminUpdateStudentComponent implements OnInit {

  
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;
  id:any;
  student:Student;
  url: any; 
  msg = "";
  profileFile:any;
  certificateFile:any;

  perAddress:Address;
  currAddress:Address;
  courseList:any;

  studentCourse:StudentCourse;


  constructor(private observer: BreakpointObserver,private route : ActivatedRoute,private studentService :StudentService
    ,private router : Router, private modalService: NgbModal,private studentCourseService: StudentCourseService,
    private courseService: CourseService) { 
    this.student = new Student();
    this.perAddress = new Address();
    this.currAddress = new Address();
    this.studentCourse = new StudentCourse();

  }

  ngOnInit(): void {
    this.id=this.route.snapshot.params['id'];
    this.studentService.getStudentById(this.id).subscribe((data)=>console.log(data));
    this.studentService.getStudentById(this.id).subscribe((data)=>this.student=data);

    this.courseService.getAllCourse().subscribe((data)=>this.courseList=data);
    this.studentService.getAllAddress().subscribe((data)=>console.log(data));

  }


  ngAfterViewInit() {
    this.observer
      .observe(['(max-width: 800px)'])
      .pipe(delay(1))
      .subscribe((res) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });
  }

 updateStudent(){
   console.log(this.courseList);
   console.log(this.student);

  }

  selectFile(event: any) { 
		if(!event.target.files[0] || event.target.files[0].length == 0) {
			this.msg = 'You must select an image';
			return;
		}
		var mimeType = event.target.files[0].type;
		if (mimeType.match(/image\/*/) == null) {
			this.msg = "Only images are supported";
			return;
		}
		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		reader.onload = (_event) => {
			this.msg = "";
			this.url = reader.result; 
      this.profileFile = event.target.files[0];

      console.log(this.profileFile);
     
		}
	}
// Certificate File
onFileSelected(event:any){
  console.log("In select event function")
  if (event.target.files.length > 0) {
  const file = event.target.files[0];
  this.certificateFile = file;
  console.log(this.certificateFile);

 }


}

submit(){
  // console.log(this.student);
  this.student.address?.push(this.currAddress);
  this.student.address?.push(this.perAddress);
 
  console.log(this.student);

  // let that = this;
  // Swal.fire('Success', 'New Student Added!', 'success').then(function(){
  //   that.router.navigate(['/studentDetails']);
  // });

  // const formData : FormData = new FormData();
  // formData.append('profile',this.profileFile);
  // formData.append('certificate',this.certificateFile);
  // formData.append('student',new Blob([JSON
  // .stringify(this.student)],{
  //   type:'application/json'
  // }));

  // this.studentService.addStudent(formData).subscribe({
  //   next(data:any){

  //   },
  //   error(data: {error:{description: string;};}):any{
  //     that.router.navigate(['/addStudent']);
  //   }
  // })



 }
 openVerticallyCentered(content:any) {
  this.modalService.open(content, { centered: true });
}

addCourse(){
  this.studentCourse.studentId=this.student.id;
  console.log(this.studentCourse);

  
  let that = this;
  this.studentCourseService.addStudentCourse(this.studentCourse)
    .subscribe({
      next(data: { description: any; }) {
        Swal.fire('Success', 'Course Added!', 'success').then(function(){
          // window.location.reload();
        });
        // that.router.navigate(['/addNotice']);
      },
      error(data: { error: { description: string; }; }): any {
        // that.router.navigate(['/addNotice']);
      }
    });
}






}

