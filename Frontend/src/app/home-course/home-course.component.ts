import { Component, OnInit } from '@angular/core';
import { CourseService } from '../service/course.service';

@Component({
  selector: 'app-home-course',
  templateUrl: './home-course.component.html',
  styleUrls: ['./home-course.component.css']
})
export class HomeCourseComponent implements OnInit {

  courseList:any;
  constructor(private courseService: CourseService) { }

  ngOnInit(): void {
    this.courseService.getAllCourse().subscribe((data)=>this.courseList=data);
  }

}
