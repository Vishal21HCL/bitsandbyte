
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { AddCourseComponent } from './add-course/add-course.component';
import { AddMaterialComponent } from './add-material/add-material.component';
import { AddNoticeComponent } from './add-notice/add-notice.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { AdminBlogComponent } from './admin-blog/admin-blog.component';

import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminMessageComponent } from './admin-message/admin-message.component';
import { AdminUploadMaterialComponent } from './admin-upload-material/admin-upload-material.component';
import { AdminViewCourseComponent } from './admin-view-course/admin-view-course.component';

import { BlogComponent } from './blog/blog.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { CreateBlogComponent } from './create-blog/create-blog.component';
import { HomeCourseComponent } from './home-course/home-course.component';


import { HomeNavbarComponent } from './home-navbar/home-navbar.component';
import { HomepageContactComponent } from './homepage-contact/homepage-contact.component';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginComponent } from './login/login.component';
import { MessageComponent } from './message/message.component';
import { PendingBlogComponent } from './pending-blog/pending-blog.component';
import { AdminGuard } from './service/admin.guard';
import { UserGuard } from './service/user.guard';
import { StudentBlogComponent } from './student-blog/student-blog.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';
import { StudentDetailsComponent } from './student-details/student-details.component';
import { StudentLoginComponent } from './student-login/student-login.component';
import { StudentMaterialComponent } from './student-material/student-material.component';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { UpdateStudentComponent } from './update-student/update-student.component';
import { UserBlogComponent } from './user-blog/user-blog.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserPendingBlogComponent } from './user-pending-blog/user-pending-blog.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { StudentCourseComponent } from './student-course/student-course.component';
import { AdminUpdateStudentComponent } from './admin-update-student/admin-update-student.component';
import { AdminViewStudentComponent } from './admin-view-student/admin-view-student.component';



const routes: Routes = [
  {path:'',redirectTo:'homepage',pathMatch:'full'},
  {path:'homepage',component:HomepageComponent},
  {path:'homeContact',component:HomepageContactComponent},
  {path:'aboutus',component:AboutUsComponent},
  {path:'login',component:LoginComponent},
  {path:'homeCourse',component:HomeCourseComponent},



  {path:'userLogin',component:UserLoginComponent},
  {path:'adminDashboard',component:AdminDashboardComponent, canActivate:[AdminGuard]},
  {path:'addStudent',component:AddStudentComponent},
  {path:'studentDetails',component:StudentDetailsComponent},
  {path:'blog',component:BlogComponent},
  {path: 'updateStudent',component:UpdateStudentComponent},
  {path: 'pendingBlog',component:PendingBlogComponent},
  {path: 'userSignUp',component:UserRegistrationComponent},
  {path:'addMaterial', component:AddMaterialComponent},
  {path:'adminMessage',component:AdminMessageComponent},
  {path:'contactForm',component:ContactFormComponent},
  {path:'addNotice',component:AddNoticeComponent},
  {path:'addCourse',component:AddCourseComponent},
  {path:'adminViewCourse',component:AdminViewCourseComponent},
  {path:'adminUploadMaterial/:id',component:AdminUploadMaterialComponent},
  {path:'adminBlog',component:AdminBlogComponent},
  {path:'updateStudent/:id',component:AdminUpdateStudentComponent},
  {path:'viewStudent/:id',component:AdminViewStudentComponent},




  // Student Panel
  {path:'userDashboard',component:UserDashboardComponent, canActivate:[UserGuard]},
  {path:'userProfile',component:UserProfileComponent},
  {path:'message',component:MessageComponent},
   {path:'studentProfile',component:StudentProfileComponent},
  {path:'message',component:MessageComponent},
  {path:'studentMaterial',component:StudentMaterialComponent},
  {path:'studentBlog',component:StudentBlogComponent},
  {path:'studentLogin',component:StudentLoginComponent},



//User Panel

  {path:'userBlog',component:UserBlogComponent},
  {path:'userPendingBlog',component:UserPendingBlogComponent},
  {path:'userDashboard',component:UserDashboardComponent},
  {path:'userProfile',component:UserProfileComponent},
  {path:'createBlog',component:CreateBlogComponent},

  // Student Panel
 
  {path:'studentDashboard',component:StudentDashboardComponent},
  {path:'studentProfile',component:StudentProfileComponent},
  {path:'message',component:MessageComponent},
  {path:'studentMaterial',component:StudentMaterialComponent},
  {path:'studentBlog',component:StudentBlogComponent},
  {path:'studentCourse',component:StudentCourseComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
