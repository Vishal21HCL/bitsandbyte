import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { delay } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Student } from '../model/Student';
import { StudentMessage } from '../model/StudentMessage';
import { StudentMessageService } from '../service/student-message.service';

@Component({
  selector: 'app-admin-message',
  templateUrl: './admin-message.component.html',
  styleUrls: ['./admin-message.component.css']
})
export class AdminMessageComponent implements OnInit {


  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;
  msg:StudentMessage = new StudentMessage();
  idd:any;
  
  studentMessage: any;
  editProfileForm!: FormGroup;
  StudentMsg: StudentMessage = new StudentMessage();


  constructor(private observer: BreakpointObserver,private studentMessageService: StudentMessageService
    ,private modalService: NgbModal,private fb: FormBuilder,private router: Router) {
    
  
    }

  ngOnInit(): void {
  
    this.studentMessageService.getMessage().subscribe((data)=>this.studentMessage=data);

    this.editProfileForm = this.fb.group({
      id:[''],
      userId: [''],
      userName: [''],
      studentMessage: [''],
      reply: ['']
     });
  }
  ngAfterViewInit() {
    this.observer
      .observe(['(max-width: 800px)'])
      .pipe(delay(1))
      .subscribe((res) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });
  }

  openVerticallyCentered(content:any,message:any) {
    this.modalService.open(content, { centered: true });
     console.log(message);

    
    // document.getElementById('userId')?.setAttribute('value',message.userId);
    // document.getElementById('userName')?.setAttribute('value',message.userName);
    // document.getElementById('studentMessage')?.setAttribute('value',message.studentMessage);
    // document.getElementById('reply')?.setAttribute('value',message.reply);

    

    // this.StudentMsg.id=message.id;
    // this.formValue.controls['userId'].setValue(message.userId);
    // this.formValue.controls['userName'].setValue(message.userName);
    // this.formValue.controls['studentMessage'].setValue(message.studentMessage);
    // this.formValue.controls['reply'].setValue(message.reply);

    this.editProfileForm.patchValue({
      id:message.id,
      userId: message.userId,
      userName: message.userName,
      studentMessage: message.studentMessage,
      reply: message.reply
     });

    
  }

  
  // this.api.updateEmployee(this.employeeModelObj,this.employeeModelObj.id)
  // .subscribe(res=>{
  //   alert("Updated Sucessfully");
  //   let ref = document.getElementById("cancel")
  //   ref?.click();
  //   this.formValue.reset();
  //   this.getAllEmployee();
  // })

  updateMessage(){
    Swal.fire({
      position: 'top',
      icon: 'success',
      title: 'Your have replied a message',
      showConfirmButton: false,
      timer: 1500
    }).then(function(){
        window.location.reload();
    })
   
    this.msg=this.editProfileForm.getRawValue();
    this.idd=this.msg.id;

    console.log(this.idd);
    this.studentMessageService.editStudentMessage(this.idd,this.msg).subscribe(res=>{
    

    })

    this.modalService.dismissAll();
    
  
    

  }



}
