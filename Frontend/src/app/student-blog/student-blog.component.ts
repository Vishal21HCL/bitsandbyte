import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { delay } from 'rxjs/operators';
import { BlogService } from '../service/blog.service';

@Component({
  selector: 'app-student-blog',
  templateUrl: './student-blog.component.html',
  styleUrls: ['./student-blog.component.css']
})
export class StudentBlogComponent implements OnInit {

  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  blogList: any;

  constructor(private observer: BreakpointObserver,private blogService : BlogService) {
  
  }
  ngOnInit(): void {
    this.blogService.getAllBlog().subscribe((data)=>this.blogList=data);
  }
  ngAfterViewInit() {
    this.observer
      .observe(['(max-width: 800px)'])
      .pipe(delay(1))
      .subscribe((res) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });
  }


}
