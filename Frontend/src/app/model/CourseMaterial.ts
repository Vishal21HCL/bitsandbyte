import { Course } from "./Course";

export class CourseMaterial{
    
  
    public id?:number;
    public title?:string;
    public description?:string;
    public materialUrl?:string;
    public course?: Course; 
    
    constructor(course:Course){
        this.course = course;

    }

}