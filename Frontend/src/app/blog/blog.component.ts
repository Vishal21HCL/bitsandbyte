import { Component, OnInit } from '@angular/core';
import {  ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { delay } from 'rxjs/operators';
import { BlogService } from '../service/blog.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

 
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;
  blogList:any;
  id:any;
 

  constructor(private observer: BreakpointObserver,private blogService : BlogService
    ,private userService : UserService) {

  }
  ngOnInit(): void {
    this.blogService.getAllBlog().subscribe((data)=>(console.log(data)));
   

    this.blogService.getAllBlog().subscribe((data) => this.blogList=data);
   
    
    
    

  }
  
  
  ngAfterViewInit() {
    this.observer
      .observe(['(max-width: 800px)'])
      .pipe(delay(1))
      .subscribe((res) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });
  }


}
