import { TestBed } from '@angular/core/testing';

import { StudentMessageService } from './student-message.service';

describe('StudentMessageService', () => {
  let service: StudentMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
