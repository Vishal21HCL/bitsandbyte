import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
 import baseUrl from './helper';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private router: Router, private http: HttpClient) { }


  public addStudent(formData: FormData) {
    return this.http.post(`${baseUrl}/student/registerStudent/`, formData, { responseType: 'text' });

  }


  // Add Student





  // logout

  logout() {
    localStorage.removeItem('studentSr');

  }

  // GET STUDENT DETAILS BY STUDENT SERIAL NUMBER

  getStudentByStudentSr(studentSr: string) {
    return this.http.get(`${baseUrl}/student/getStudentBySr/` + studentSr);
  }

  // get all student details

  getAllStudent(){
    return this.http.get(`${baseUrl}/student/getStudent`);
   }

  //  delete Student

  deleteStudentById(id:number){
    return this.http.delete(`${baseUrl}/student/deleteStudent/`+id);
  }

  getStudentById(id: any) {
    return this.http.get(`${baseUrl}/student/getStudentById/` + id);
  }


  // get All Address
  getAllAddress(){
    return this.http.get(`${baseUrl}/student/getAddress`);
   }
  






}
