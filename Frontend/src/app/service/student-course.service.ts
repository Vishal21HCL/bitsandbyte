import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { StudentCourse } from '../model/StudentCourse';
import baseUrl from './helper';

@Injectable({
  providedIn: 'root'
})
export class StudentCourseService {

  constructor(private router: Router, private http: HttpClient) { }


  addStudentCourse(studentCourse: StudentCourse):  Observable<any> {
    return this.http.post<any>(`${baseUrl}/studentCourse/addCourse`, studentCourse);
  }
}
